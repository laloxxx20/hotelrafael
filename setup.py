#!/usr/bin/env python

from setuptools import setup

setup(
    name='HotelRafael',
    version='1.0',
    description='OpenShift App',
    author='eduardo chauca',
    author_email='example@example.com',
    url='http://www.python.org/sigs/distutils-sig/',
    install_requires=['Django==1.5.1'],
)
