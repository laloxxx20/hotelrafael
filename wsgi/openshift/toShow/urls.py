from django.conf.urls import patterns, url
from toShow import views
from django.contrib.auth.decorators import login_required, user_passes_test

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^servicios/$', views.services, name='services'),
	url(r'^habitaciones/$', views.rooms, name='rooms'),
	url(r'^galeria/$', views.gallery, name='gallery'),
	url(r'^turismo/$', views.turism, name='tourism'),
	url(r'^contactenos/$', views.contact, name='contact'),	
)
