# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from toShow.models import ImagesData

def index(request):	
	context = {'myPlace':'Inicio','menu':'index'} # addhering to the dictionery  (general)context new data, the place that i am
	return render(request,'index.html',context)	

def services(request):
	context = {'myPlace':'Servicios','menu':'services'}
	return render(request,'services.html',context)	

def rooms(request):
	context = {'myPlace':'Habitaciones','menu':'rooms'} 
	return render(request,'rooms.html',context)		

def gallery(request):
	allDataImages=ImagesData.objects.all()
	context = {'myPlace':'Galería','menu':'gallery','dataImages':allDataImages}  
	return render(request,'gallery.html',context)		

def turism(request):
	context = {'myPlace':'Turismo','menu':'tourism'} 
	return render(request,'tourism.html',context)		

def contact(request):
	context = {'myPlace':'Contacto','menu':'contact'} 
	return render(request,'contact.html',context)					