/**************************
*
*	EXT - for the quotes
*
**************************/
(function($){
	$.fn.extend({ 
        //plugin name - rotaterator
        rotaterator: function(options) {

        	var defaults = {
        		fadeSpeed: 600,
        		pauseSpeed: 100,
        		child:null
        	};

        	var options = $.extend(defaults, options);

        	return this.each(function() {
        		var o =options;
        		var obj = $(this);                
        		var items = $(obj.children(), obj);
        		items.each(function() {$(this).hide();})
        		if(!o.child){var next = $(obj).children(':first');
        	}else{var next = o.child;
        	}
        	$(next).fadeIn(o.fadeSpeed, function() {
        		$(next).delay(o.pauseSpeed).fadeOut(o.fadeSpeed, function() {
        			var next = $(this).next();
        			if (next.length == 0){
        				next = $(obj).children(':first');
        			}
        			$(obj).rotaterator({child : next, fadeSpeed : o.fadeSpeed, pauseSpeed : o.pauseSpeed});
        		})
        	});
        });
        }
    });
})(jQuery);

$(function(){

   	/**************************
	*
	*	for the booking forms
	*
	**************************/
	$('.room_selector .pull-right').bind('click', function() {
		
		var no_slides = $('.room_selector').length;
		var parent = $(this).parent().parent();
		var current = $('.room_selector').index(parent);
		var next = 0;
		if(current < no_slides-1){
			next = current + 1;
		}
		$('.room_selector').hide();
		$('.room_selector:eq('+next+')').show();
		update_total_price();
		return false;
	});

	$('.room_selector .pull-left').bind('click', function() {
		
		var no_slides = $('.room_selector').length;
		var parent = $(this).parent().parent();
		var current = $('.room_selector').index(parent);
		var prev = no_slides-1;
		if(current > 0){
			prev = current - 1;
		}
		$('.room_selector').hide();
		$('.room_selector:eq('+prev+')').show();
		update_total_price();
		return false;
	});

	$('.home .datepicker_from').datepicker({                
		dateFormat: 'dd/mm/yy',
		showOtherMonths: true,
		numberOfMonths: 1,
		altField: ".check-in-date",
		altFormat: "d M, '3:00 PM'",
		minDate: 0,
		onSelect: function(date) {
			update_total_price();
		}
	});	
	$('.home .datepicker_to').datepicker({                
		dateFormat: 'dd/mm/yy',
		showOtherMonths: true,
		numberOfMonths: 1,
		altField: ".check-out-date",
		altFormat: "d M, '12:00 PM'",
		minDate: 1,
		defaultDate: "+1w",
		onSelect: function(date) {
			update_total_price();
		}
	});


	$('.book-start .datepicker_from').datepicker({   
		changeMonth: true,
		numberOfMonths: 3,
		minDate: 0,
		onClose: function( selectedDate ) {
			$('.book-start .datepicker_from').datepicker( "option", "minDate", selectedDate );
		},
		onSelect: function(date) {
			update_total_price();
		}
	});
	$('.book-start .datepicker_to').datepicker({       
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		minDate: 1,
		onClose: function( selectedDate ) {
			$('.book-start .datepicker_to').datepicker( "option", "maxDate", selectedDate );
		},
		onSelect: function(date) {
			update_total_price();
		}
	});

	$('.global-datepicker-from').datepicker({   
		minDate: 0,
		onClose: function( selectedDate ) {
			$('.global-datepicker-from').datepicker( "option", "minDate", selectedDate );
		}
	});
	$('#global-datepicker-from').click(function(){
		$('.global-datepicker-from').datepicker('show');
		return false;
	});
	$('.global-datepicker-to').datepicker({   
		defaultDate: "+1w",
		minDate: 1,
		onClose: function( selectedDate ) {
			$('.global-datepicker-to').datepicker( "option", "maxDate", selectedDate );
		}
	});
	$('#global-datepicker-to').click(function(){
		$('.global-datepicker-to').datepicker('show');
		return false;
	});
	var firstDay = new Date();
	var nextWeek = new Date(firstDay.getTime() + 7 * 24 * 60 * 60 * 1000);
	$(".global-datepicker-from").datepicker('setDate', firstDay);
	$(".global-datepicker-to").datepicker('setDate', nextWeek);
	$('.select_rooms').bind('change', function(event){
		update_total_price();
	});

	if($('.booking_summary').length > 0) {
		$('html, body').animate({scrollTop: 20}, "fast");
	}

	if($('.book-pay').length > 0) {
		update_extras_price();
		$('[type=checkbox]').bind('click', function() {
			update_extras_price();
		});
	}

	/**************************
	*
	*	for the quotes
	*
	**************************/
	$('#quotes').rotaterator({fadeSpeed:1000, pauseSpeed:3000});
	$('.btn-info').bind('click', function() {
		$(this).toggleClass('active');
		return false;
	});
	
	/**************************
	*
	*	for the gallery
	*
	**************************/
	$('#source').quicksand( $('#destination li') );	

	// get the first collection
	var $gallery = $('#gallery');

	// clone gallery to get a second collection
	var $data = $gallery.clone();

	// attempt to call Quicksand on every form change
	$('.gallery-pills a').bind('click', function() {
		var filterType = $(this).attr('href').substring(1);
		if (filterType == 'all') {
			var $filteredData = $data.find('li');
		} else {
			var $filteredData = $data.find('li[data-type=' + filterType + ']');
		}


		// finally, call quicksand
		$gallery.quicksand($filteredData, {
			duration: 800,
			easing: 'swing'
		});

	});


	initialize_map();


});

/**************************
*
*	start slider
*
**************************/
jQuery(window).load(function(){
	jQuery("#nivoslider-125").nivoSlider({
		effect:"random",
		slices:15,
		boxCols:8,
		boxRows:4,
		animSpeed:500*3,
		pauseTime:3000,
		startSlide:0,
		directionNav:true,
		controlNav:true,
		controlNavThumbs:false,
		pauseOnHover:true,
		manualAdvance:false
	});
	$('.slider-wrapper').css('min-height', 'auto');
	$('.nivoSlider').css('min-height', 'auto');
});

// DOMContentLoaded
$(function() {



	/*if($('.home').length > 0) {
		var position_top = $('.room_selector').position().top+50;
		var position_left = $('.book-now').position().left;
		$('#promotion_box').hide();
		$('#promotion_box').css({'left' : position_left+250, 'top' : position_top});


		/*$(document).mousemove(function(e){ 
		    if(mouseLastYPos){ 
		    	
		        if (mouseLastYPos > $('.room_selector').position().top){
		           $('#promotion_box').show();		    
		        } else {
		        	$('#promotion_box').hide();		    
		        }
		    }
		    mouseLastYPos = e.pageY;
		});*/
	//}
/*
	$(".home").hover(
		function () {
			var position_left = $('.book-now').position().left;
			$('#promotion_box').animate({
				left: position_left+250
			}, 1000, function() {
				// Animation complete.
			});
		},
		function () {
			$('#promotion_box').css({'left' : $(window).width()+$('#promotion_box').width()});
		}
		);*/



});

/**************************
*
*	begin functions
*
**************************/
function update_extras_price() {
	var base_price = 1280;
	var extras = $('input:checked').length;
	var extras_price = (extras*15);
	var total_price = base_price + extras_price;
	$('#extras_price').html(extras_price.toFixed(2) + " GBP");
	$('#total_price').html(total_price.toFixed(2) + " GBP");
}

function update_total_price() {
	if($('.room_selector').length == 0)
		return false;
	var room = $('.room_selector:visible');
	//first we update the select boxes
	var max_adults = parseInt(room.data('adults'));
	var max_kids = parseInt(room.data('kids'));
	var adults_options = [];
	var kids_options = [0];
	for(var i = 1; i <= max_adults; i++){
		adults_options.push(i);
	}	
	for(var i = 1; i <= max_kids; i++){
		kids_options.push(i);
	}

	var $subType = $(".select_adults");
	$subType.empty();
	$.each(adults_options, function () {
		$subType.append($('<option></option>').attr("value", this).text(this));
	});

	var $subType = $(".select_kids");
	$subType.empty();
	$.each(kids_options, function () {
		$subType.append($('<option></option>').attr("value", this).text(this));
	});

	var price = parseInt(room.data('price'));
	var total_price = price;

	var days = returnNumberOfDaysBetweenTwoDates($('.datepicker_from').datepicker( "getDate" ), $('.datepicker_to').datepicker( "getDate" ));
	total_price = total_price * days * parseInt($('.select_rooms').val());

	$('#total_price').html(total_price.toFixed(2) + " GBP");
	$('#total_price').css('letterSpacing', '0px');
	if(total_price < 100) {
		$('#total_price').css('letterSpacing', '2px');
	}
	if(total_price >= 1000) {
		$('#total_price').css('letterSpacing', '-2px');
	}
	if(total_price >= 10000) {
		$('#total_price').css('letterSpacing', '-4px');
	}
	
}
function returnNumberOfDaysBetweenTwoDates(date1, date2) {
	var minutes = 1000*60;
	var hours = minutes*60;
	var days = hours*24;

	var diff = Math.abs(date1.getTime() - date2.getTime());

	return Math.round(diff / days);
}
var dates = {'2012/11/22':'some description' , '2012/11/30':'some other description'}; 
var tips  = ['some description','some other description']; 
function highlightDays(date) {
	var search = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + (date.getDate());
	if (dates[search]) {
		return [true, 'highlight', dates[search] || ''];
	}
	return [false, '', ''];
} 

/**************************
*
*	Google Maps API
*
**************************/
function initialize() {
	var myLatlng =new google.maps.LatLng( -16.62308989,-72.70863801);
	var mapOptions = {
		zoom: 16,
		center: myLatlng,		
		// scrollwheel: false,
		// navigationControl: false,
		// scaleControl: false,
		// streetViewControl: false,
		// draggable: true, 
		// mapTypeControl: true,
		// mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
		// navigationControl: true,
		// navigationControlOptions: {style: google.maps.NavigationControlStyle.BIG},
		// mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	var marker = new google.maps.Marker({
	  position: myLatlng,
	  map: map,
	  title: 'Hotel Rafael I - Avenida Quilca 255',	  
	});
}

function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&' +
	  'callback=initialize';
	document.body.appendChild(script);
}

window.onload = loadScript;